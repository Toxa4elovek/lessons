<?php
/**
 *
 */

define('LINE_BREAK', '<br>');
define('DELIMITER', ', ');
define('DAYS_COUNT', 365);
define('MONTH_COUNT', 12);
define('DAY_SECONDS', 86400);