<?php

namespace homework\first\classes;

/**
 * Class Profile
 * @package homework\first\classes
 *
 * @property \DateTime $birthDay
 */
class Profile
{
    //Дата рождения
    private $birthDay;

    /**
     * Profile constructor.
     * @param \DateTime $birthday
     */
    public function __construct(\DateTime $birthday)
    {
        $this->birthDay = $birthday;
    }

    /**
     * Возвращает метку времени даты рождения
     * @return false|int
     */
    private function getBirthdayTimestamp():int
    {
        return $this->birthDay->getTimestamp();
    }

    /**
     * Метод возвращает количество дней  прошедших от даты рождения
     * @return int
     */
    public function getDaysCountFromBirthday():int
    {
        return floor((time() - $this->getBirthdayTimestamp()) / DAY_SECONDS);
    }

    /**
     * @return int
     */
    public function getAge():int
    {
        $difference = $this->getDateDifference();

        return (int)$difference->y;
    }

    /**
     * @return bool|\DateInterval
     */
    private function getDateDifference():\DateInterval
    {
        $nowDate = new \DateTime();

        return $nowDate->diff($this->birthDay);
    }

    /**
     * @return string
     */
    public function getMonthCountFromBirthday():string
    {
        $years                 = $this->getAge();
        $mothDiffOfCurrentYear = $this->getDateDifference()->m;

        return $years * 12 + $mothDiffOfCurrentYear;
    }

    /**
     * Метод устанавливает значение даты рождения
     * @param \DateTime $value
     */
    public function setBirthDay(\DateTime $value):void
    {
        $this->birthDay = $value;
    }
}