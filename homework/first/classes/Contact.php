<?php

namespace homework\first\classes;

/**
 * Class Contact
 * @package lessons\homework\first\classes
 *
 * @property string $city
 * @property string $street
 * @property string $houseNumber
 *
 */
class Contact
{

    private $city;
    private $street;
    private $houseNumber;

    /**
     * Contact constructor.
     * @param string $city
     * @param string $street
     * @param int $houseNumber
     */
    public function __construct(string $city, string $street, int $houseNumber)
    {
        $this->city        = $city;
        $this->street      = $street;
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function __toString():string
    {
        return $this->getAddress();
    }

    /**
     * @return string
     */
    public function getAddress():string
    {
        return $this->city . DELIMITER . $this->street . DELIMITER . $this->houseNumber;
    }

    /**
     * @param string $value
     */
    public function setCity(string $value):void
    {
        $this->city = $value;
    }

    /**
     * @param string $value
     */
    public function setStreet(string $value):void
    {
        $this->street = $value;
    }

    /**
     * @param int $value
     */
    public function setHouseNumber(int $value):void
    {
        $this->houseNumber = $value;
    }
}