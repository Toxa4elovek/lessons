<?php
/**
 * Домашнее задание №1
 */
require_once '../../vendor/autoload.php';

use homework\first\classes\Contact;
use homework\first\classes\Profile;


$contact = new Contact('Донецк', 'Красногвардейский пр-т', '26');

echo $contact . LINE_BREAK;

print $contact . LINE_BREAK;

$birthDate = new \DateTime('1992-07-29');
$profile = new Profile($birthDate);
//Возраст
echo 'Возраст = ' . $profile->getAge() . LINE_BREAK;
//Количество месяцев от даты рождения
echo 'Количество месяцев от даты рождения = ' . $profile->getMonthCountFromBirthday() . LINE_BREAK;
//Количество дней от даты рождения
echo 'Количество дней от даты рождения = ' . $profile->getDaysCountFromBirthday() . LINE_BREAK;

//Удаление переменных
unset($contact, $address, $profile);